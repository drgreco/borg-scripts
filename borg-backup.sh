#!/usr/bin/env bash

export BACKUP_SERVER=borg.mjjlv.com
export BACKUP_USER=backup
export BACKUP_PATH_BASE=/srv/backup/hosts
export HOSTNAME=$(hostname -f)
export BORG_IGNORE_FILE=.borg.ignore
export QUIET=false

usage() {
  echo "Usage: $0 -r REPO [-q]" 1>&2 
  exit 1
}


if [ "${USER}" != "root" ]; then
    echo "Error: must run as root"
    usage
fi

while getopts r:q option
do
	case "${option}"
	in
		r) REPO=${OPTARG};;
    q) QUIET=true;;
	esac
done

if [ "${REPO}" == "" ]; then
	usage
	exit 1
fi

export BORG_REPO=${BACKUP_USER}@${BACKUP_SERVER}:${BACKUP_PATH_BASE}/${HOSTNAME}/${REPO}

info() { 
    if [ "${QUIET}" == "false" ]; then
        printf "\n%s %s\n\n" "$( date )" "$*" >&2; 
    fi
}
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

export EXCLUDE_OPTIONS=$(find /home -maxdepth 2 -name ${BORG_IGNORE_FILE} -exec echo --exclude-from {} \;) 

info "Starting backup"

export NOW=$(date "+%Y%m%d%H%M.%S")

if [ "${QUIET}" == "false" ]; then
    export EXTRA_ARGS="--list --stats --verbose"
fi

borg create                         	\
    --filter AME                    	\
    ${EXTRA_ARGS}                    	\
    --show-rc                       	\
    --compression lz4               	\
    --exclude-caches                	\
    ${EXCLUDE_OPTIONS}              	\
    ::${HOSTNAME}-${NOW}	            \
    /etc                            	\
    /home			    	                  \

backup_exit=$?

info "Pruning repository"
borg prune                          \
    ${EXTRA_ARGS}                  	\
    --prefix '{hostname}-'          \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  12              \
    --keep-yearly   3		            \
    ${BORG_REPO}

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 1 ];
then
    info "Backup and/or Prune finished with a warning"
fi

if [ ${global_exit} -gt 1 ];
then
    info "Backup and/or Prune finished with an error"
fi

exit ${global_exit}
